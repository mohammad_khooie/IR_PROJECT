from xml.etree import ElementTree
from elasticsearch import Elasticsearch
from os import listdir
from os.path import isfile,join


## Check these URLs:
## https://elasticsearch-py.readthedocs.io/en/master/
## https://tryolabs.com/blog/2015/02/17/python-elasticsearch-first-steps/
## https://stackoverflow.com/questions/1912434/how-do-i-parse-xml-in-python


es = Elasticsearch([{'host': 'localhost', 'port': 9200}])


def start():
    path = ".\Hamshahri\documents"
    files_list = [join(path,f_n) for f_n in listdir(path) if isfile(join(path,f_n))]
    for file in files_list:
        tree = ElementTree.parse(file)
        root = tree.getroot()
        print(root)
        docs = tree.findall('.//DOC')
        for doc in docs:
            docno = doc.find('DOCNO').text
            title = doc.find('TITLE').text
            text = doc.find('TEXT').text
            all_dates = doc.findall('DATE')
            date = ''
            for d in all_dates:
                if d.attrib["calender"] == "Western":
                    date = d.text
                    break

            doc = {
                'docno':docno,
                'title': title,
                'text': text,
                'date': date
            }
            insert_to_elastic(doc)


def insert_to_elastic(doc):
    es.index(index='hamshahri', doc_type='document', body=doc)


start()



