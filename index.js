const elasticsearch = require( "elasticsearch" );
const fs = require( "fs" );
const readLine = require( "readline" );

// اتصال به پایگاه داده
const client = new elasticsearch.Client( {
    "host": "localhost:9200"
} );


// ساخت شاخص به همراه مشخص کردن نوع تحلیلگر مناسب برای زبان فارسی
const createIndex = ( callback ) => {
    client.indices.create( {
        "index": "hamshahri",
        "body": {
            "mappings": {
                "document": {
                    "properties": {
                        "date": {
                            "type": "text"
                        },
                        "docno": {
                            "type": "text"
                        },
                        "text": {
                            "type": "text"
                        },
                        "title": {
                            "type": "text"
                        }
                    }
                }
            },
            "settings": {
                "index": {
                    "analysis": {
                        "filter": {
                            "persian_stop": {
                                "type": "stop",
                                "stopwords": "_persian_"
                            }
                        },
                        "analyzer": {
                            "rebuilt_persian": {
                                "filter": [
                                    "lowercase",
                                    "decimal_digit",
                                    "arabic_normalization",
                                    "persian_normalization",
                                    "persian_stop"
                                ],
                                "char_filter": [
                                    "zero_width_spaces"
                                ],
                                "tokenizer": "standard"
                            }
                        },
                        "char_filter": {
                            "zero_width_spaces": {
                                "type": "mapping",
                                "mappings": [
                                    "\\u200C=> "
                                ]
                            }
                        }
                    }
                }
            }
        }
    }, ( error, result ) => {
        if ( error ) {
            return callback( null, error );
        } else if ( result ) {
            return callback( result, null );
        }
    } );
};


// تابعی برای دریافت تعداد اسناد
const getDocumentCount = ( callback ) => {
    client.count( { "index": "hamshahri" }, ( error, result ) => {
        if ( error ) {
            return callback( null, error );
        } else if ( result ) {
            return callback( result, null );
        }
    } );
};

//  جهت تعریف تحلیگر و تحلیل یا توکن کردن یک رشته ی ورودی
const setStopWordsAndTokenaize = ( stopWords = [], text, callback ) => {
    client.indices.analyze( {
        "index": "hamshahri",
        "body": {
            "tokenizer": "standard",
            "filter": [ { "type": "stop", "stopwords": stopWords } ],
            "text": text
        }
    }, ( error, result ) => {
        if ( error ) {
            return callback( null, error );
        } else if ( result ) {
            return callback( result, null );
        }
    } );
};

// tf و df برای یک سند مشخص دریافت
const getDFAndTF_AllTermsByDocId = ( docId, fields = [], callback ) => {
    client.termvectors( {
        "index": "hamshahri",
        "type": "document",
        "id": docId,
        "fields": fields,
        "fieldStatistics": true,
        "termStatistics": true
    }, ( error, result ) => {
        if ( error ) {
            return callback( null, error );
        } else if ( result ) {
            return callback( result, null );
        }
    } );
};

//  دریافت کلید های اصلی ایجاد شده برای تمام اسناد
const getIds = ( callback ) => {
    getDocumentCount( ( count, error ) => {
        if ( error ) {
            return callback( null, error );
        }
        client.search( {
            "index": "hamshahri",
            "type": "document",
            "body": {
                "query": {
                    "match_all": {}
                },
                "_source": [ "_id" ],
                "size": count.count,
                "from": 0
            }
        }, ( s_error, result ) => {
            let ids = [];

            if ( s_error ) {
                return callback( null, s_error );
            } else if ( result ) {
                let docs = result.hits.hits;

                for ( let doc of docs ) {
                    ids.push( doc._id );
                }
                return callback( ids, null );
            }
        } );
    } );
};

// tf و df  برای تمامی اسناد دریافت
const getDFAndTF_AllTerms_AllDocs = ( callback ) => {
    getIds( ( _ids, error ) => {
        if ( error ) {
            return callback( null, error );
        }
        let i, j, chunk_ids, chunk = 10;

        for ( i = 0, j = _ids.length; i < j; i += chunk ) {
            chunk_ids = _ids.slice( i, i + chunk );
            client.mtermvectors( {
                "index": "hamshahri",
                "type": "document",
                "ids": chunk_ids,
                "termStatistics": true,
                "fields": [ "title", "text" ]
            }, ( m_error, result ) => {
                if ( m_error ) {
                    return callback( null, m_error );
                }
                return callback( result, null );
            } );
        }
    } );
};
// tf و df و tf_idf برای تمامی اسناد دریافت
const get_DF_TF_TFIDF_AllTerms_AllDocs = ( callback ) => {
    getDFAndTF_AllTerms_AllDocs( ( result, error ) => {
        if ( error ) {
            return callback( null, error );
        }
        const dvs = result.docs;

        for ( let docObj of dvs ) {
            if ( docObj.term_vectors ) {
                if ( docObj.term_vectors.title.terms ) {
                    let titleDocVec = docObj.term_vectors.title.terms;

                    for ( let key in titleDocVec ) {
    
                        titleDocVec[ key ].tf_idf = titleDocVec[ key ].doc_freq * titleDocVec[ key ].term_freq;
                    }
                }
                if ( docObj.term_vectors.text.terms ) {
                    let textDocVec = docObj.term_vectors.text.terms;

                    for ( let key in textDocVec ) {
                        textDocVec[ key ].tf_idf = textDocVec[ key ].doc_freq * textDocVec[ key ].term_freq;
                    }
                }
            }
        }
        return callback( dvs, null );
    } );
};


// دریافت نتیجه ی جستوجو با توجه به پرس و جوی وارد شده
const search = ( query, callback ) => {
    client.search( {
        "index": "hamshahri",
        "type": "document",
        "body": {
            "query": {
                "multi_match": {
                    "query": query,
                    "fields": [ "title", "text" ]
                }
            },
            "_source": [ "docno", "title", "text", "date" ],
            "size": 20,
            "from": 0
        }
    }, ( error, result ) => {
        if ( error ) {
            return callback( null, error );
        } else if ( result ) {
            return callback( result, null );
        }
    } );
};


module.exports = { createIndex, getDocumentCount, get_DF_TF_TFIDF_AllTerms_AllDocs, setStopWordsAndTokenaize, search };

