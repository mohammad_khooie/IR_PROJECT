const app = require( "express" )(),
    bodyParser = require( "body-parser" ),
    { createIndex, getDocumentCount, get_DF_TF_TFIDF_AllTerms_AllDocs, setStopWordsAndTokenaize, search } = require( "./index" );


app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { "extended": false } ) );


app.get( "/create-index", ( req, res ) => {
    createIndex( ( result, error ) => {
        if ( error ) {
            res.statusCode = 400;
            res.setHeader( "Content-Type", "application/json" );
            res.json( { "err": error } );
            res.end();
        } else {
            res.statusCode = 200;
            res.setHeader( "Content-Type", "application/json" );
            res.json( result );
            res.end();
        }
    } );
} );


app.get( "/documents-count", ( req, res ) => {
    getDocumentCount( ( result, error ) => {
        if ( error ) {
            res.statusCode = 400;
            res.setHeader( "Content-Type", "application/json" );
            res.json( { "err": error } );
            res.end();
        } else {
            res.statusCode = 200;
            res.setHeader( "Content-Type", "application/json" );
            res.json( result );
            res.end();
        }
    } );
} );


app.get( "/get-tf-df-tfidf-allterms-alldocs", ( req, res ) => {
    get_DF_TF_TFIDF_AllTerms_AllDocs( ( result, error ) => {
        if ( error ) {
            res.status( 400 ).json( { "err": error } );
            res.end();
        } else {
            res.status( 200 ).json( result );
            res.end();
        }
    } );
} );


app.post( "/set-stopword-and-tokenaize", ( req, res ) => {
    const stopWords = req.body.stopWords;
    const text = req.body.text;

    setStopWordsAndTokenaize( stopWords, text, ( result, error ) => {
        if ( error ) {
            res.statusCode = 400;
            res.setHeader( "Content-Type", "application/json" );
            res.json( { "err": error } );
            res.end();
        } else {
            res.statusCode = 200;
            res.setHeader( "Content-Type", "application/json" );
            res.json( result );
            res.end();
        }
    } );
} );


app.post( "/search", ( req, res ) => {
    const query = req.body.query;

    search( query, ( result, error ) => {
        if ( error ) {
            res.statusCode = 400;
            res.setHeader( "Content-Type", "application/json" );
            res.json( { "err": error } );
            res.end();
        } else {
            res.statusCode = 200;
            res.setHeader( "Content-Type", "application/json" );
            res.json( result );
            res.end();
        }
    } );
} );

app.listen( 3000, () => {
    console.log( "server is running on port 3000" );
} );
